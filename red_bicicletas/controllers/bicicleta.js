var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    // res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
    Bicicleta.allBicis(function(err,bicis){
        res.render('bicicletas/index', {bicis: bicis});
    });
}

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    console.log(req.body.code);
    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici, function (err, success) {
        if(err) console.log(err);
        res.redirect('/bicicletas');
    });

    
}

exports.bicicleta_update_get = function(req, res){
    console.log(req.params.code); 
    Bicicleta.findByCode(req.params.code, function(err, bici){
        if(err) console.log(err);
        res.render('bicicletas/update', {bici});
    });
}

exports.bicicleta_update_post = function(req, res){
    Bicicleta.findByCode(req.params.code,function (err, bici) {
        bici.id = req.body.code;
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lng];

        Bicicleta.update(bici, function (err, bici) {
            res.redirect('/bicicletas');
        })

        
    });
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeByCode(req.params.code, function(err, bici){
        res.redirect('/bicicletas');
    });
}