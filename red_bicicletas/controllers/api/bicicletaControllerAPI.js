var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
};

exports.lista_Bicicletas = function (req,res){
    Bicicleta.allBicis(function(err,bicis){
        res.status(200).json({bicicletas:bicis});
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
};

exports.crear_bicicleta = function(req, res){
    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici, function (err, aBici) {
        res.status(200).json({
            bicicleta: aBici
        });
   });
};

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(req.body.code,function(err, bici){
        if(err)console.log(err);
        res.status(204).send();
    });
}


exports.bicicleta_update = function(req, res){
    Bicicleta.findByCode(req.body.code, function(err, bici){
        bici.code = req.body.code;
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lng];

        Bicicleta.update(bici, function (err, abici) {
            res.status(200).json({bicicleta:bici});
        });
    });
    
}