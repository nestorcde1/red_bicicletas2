var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

//router.get('/',bicicletaController.bicicleta_list);
router.get('/',bicicletaController.lista_Bicicletas);
//router.post('/create',bicicletaController.bicicleta_create);
router.post('/create',bicicletaController.crear_bicicleta);
router.delete('/delete',bicicletaController.bicicleta_delete);
router.put('/update',bicicletaController.bicicleta_update);

module.exports = router;