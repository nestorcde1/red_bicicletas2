var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');


describe('Testando Usuarios', function () {
    beforeAll((done) => { 
        mongoose.connection.close(done); 
    });
    beforeEach(function (done) {
        //mongoose.disconnect(done);
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if (err)console.log(err);
            Usuario.deleteMany({}, function (err, success) {
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function (err, success) {
                    if(err)console.log(err);
                    mongoose.disconnect(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', ()=>{
        it('debe existir la Reserva', (done)=>{
            const usuario = new Usuario({nombre: 'Nestor'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: 'granate', modelo: 'rural', ubicacion: [-25.539987, -54.602264] });
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, manana, function (err, reserva) {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function (err, reservas) {
                    console.log('reservas antes de comparar: '+reservas[0]);
                    //expect(reservas.length).toBe(1);
                    expect(reservas[0].diasReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});